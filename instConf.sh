#Instalacion vsftpd
apt-get update
apt-get install apache2 --yes

apt install vsftpd
systemctl enable vsftpd

useradd -m AlbertoFTP -p '$6$LlYVA4uaZyM1J39y$uR7aXbb9ChjhYJB80DnS74WuvRP672PaMdbj6FSU8gvAQl5WnlIJGr8LPc2iVPu4ULFFSeIaVTQtBLk9H1FwK0' 
cd /home/AlbertoFTP

mkdir ftp
chown nobody:nogroup /home/AlbertoFTP/ftp
chmod a-w /home/AlbertoFTP/ftp

mkdir /home/AlbertoFTP/ftp/files 
chown AlbertoFTP:AlbertoFTP /home/AlbertoFTP/ftp/files
echo "vsftpd archivo de ejemplo" | sudo tee /home/AlbertoFTP/ftp/files/ejemplo.txt

ufw allow 20/tcp; sudo ufw allow 21/tcp; sudo ufw allow 990/tcp; sudo ufw allow 40000:50000/tcp

#modificar archivo y mover ------/etc/vsftpd.conf
rm /etc/vsftpd.conf
cp /vagrant/vsftpd.conf /etc
systemctl restart vsftpd